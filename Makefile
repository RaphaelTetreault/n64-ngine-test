V=1
SOURCE_DIR=src
BUILD_DIR=build
include $(N64_INST)/include/n64.mk

N64_ROM_SAVETYPE = none
N64_ROM_REGIONFREE = true
N64_ROM_RTC = false

N64_CFLAGS += -Ilibs/libdragon-extensions/include -fdiagnostics-color=never

C_ROOT_FILES := $(wildcard src/*.c)
C_ROOT_1_FILES := $(wildcard src/**/*.c)
C_ROOT_2_FILES := $(wildcard src/**/**/*.c)
C_ROOT_3_FILES := $(wildcard src/**/**/**/*.c)
C_ROOT_4_FILES := $(wildcard src/**/**/**/**/*.c)
C_ROOT_5_FILES := $(wildcard src/**/**/**/**/**/*.c)
C_LIB_EXTENSIONS_FILES := $(wildcard libs/libdragon-extensions/src/*.c)

SRC = $(C_ROOT_FILES) $(C_ROOT_1_FILES) $(C_ROOT_2_FILES) $(C_ROOT_3_FILES) $(C_ROOT_4_FILES) $(C_ROOT_5_FILES) $(C_LIB_EXTENSIONS_FILES)
OBJS = $(SRC:%.c=%.o)
DEPS = $(SRC:%.c=%.d)

all: hello_ngine.z64

hello_ngine.z64: N64_ROM_TITLE="Hello NGine"
$(BUILD_DIR)/hello_ngine.elf: $(OBJS)

clean:
	find . -name '*.v64' -delete
	find . -name '*.z64' -delete
	find . -name '*.elf' -delete
	find . -name '*.o' -delete
	find . -name '*.d' -delete
	find . -name '*.bin' -delete
	find . -name '*.plan_bak*' -delete
	find ./src -name '*.sprite' -delete
	find . -name '*.dfs' -delete
	find . -name '*.raw' -delete
	find . -name '*.z64' -delete
	find . -name '*.n64' -delete
	find . -name '*.gen.c' -delete
	find . -name '*.gen.h' -delete
	rm -rf build/

-include $(DEPS)

.PHONY: all clean