#include "hello_world.script.h"

#include "../game.gen.h"

void script_hello_world_create() {
}

short script_hello_world_tick() {
	printf("hello_world!\n");
	return -1;
}

void script_hello_world_display(display_context_t disp) {
}

void script_hello_world_destroy() {
}