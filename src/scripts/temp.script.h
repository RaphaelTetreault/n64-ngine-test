#pragma once

#include <libdragon.h>

void script_temp_create();
short script_temp_tick();
void script_temp_display(display_context_t disp);
void script_temp_destroy();
