#include "print_hello.script.h"

#include "../game.gen.h"

void script_print_hello_create() {
}

short script_print_hello_tick() {
	return -1;
}

void script_print_hello_display(display_context_t disp) {
	//printf("print_hello!\n");
	graphics_set_color(0x000000FF, 0x00000000);
	graphics_draw_text(disp, 16, 16, "print hello gfx\n");
}

void script_print_hello_destroy() {
}