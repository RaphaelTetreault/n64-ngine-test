#pragma once

#include <libdragon.h>

void script_hello_world_create();
short script_hello_world_tick();
void script_hello_world_display(display_context_t disp);
void script_hello_world_destroy();
