#pragma once

#include <libdragon.h>

void script_print_hello_create();
short script_print_hello_tick();
void script_print_hello_display(display_context_t disp);
void script_print_hello_destroy();
